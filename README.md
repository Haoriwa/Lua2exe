- [Lua2exe](#lua2exe)
- [前言](#前言)
- [1. 环境搭建](#1-环境搭建)
- [2. VSCode的安装](#2-vscode的安装)
- [3. mingw64的安装](#3-mingw64的安装)
  - [3.1 mingw64的下载](#31-mingw64的下载)
  - [3.2 mingw64的安装](#32-mingw64的安装)
- [4. Lua的安装及配置](#4-lua的安装及配置)
  - [4.1 Lua的下载](#41-lua的下载)
  - [4.2 Lua的安装](#42-lua的安装)
  - [4.3 Lua库的下载](#43-lua库的下载)
- [5. IUP库](#5-iup库)
- [6. srlua](#6-srlua)
  - [6.1 下载](#61-下载)
  - [6.2 编译](#62-编译)
    - [6.2.1 编译glue](#621-编译glue)
    - [6.2.2 编译srlua](#622-编译srlua)
      - [6.2.2.1 简单粗暴的方法](#6221-简单粗暴的方法)
      - [6.2.2.2 "文明"一点的方法](#6222-文明一点的方法)
- [7.实例](#7实例)
  - [7.1 Lua脚本文件编写](#71-lua脚本文件编写)
  - [7.2 生成exe](#72-生成exe)
- [后记](#后记)

# Lua2exe
Lua脚本转exe可执行文件过程记录，及相关文件。

# 前言

在Windows环境下的开发中环境变量的设置太重要了！

# 1. 环境搭建
代码编辑器：VSCode

编译器：gcc(mingw64)

编译终端：PowerShell(Windows自带)

开发语言：Lua

相关库及开源工具：Lua, IUP , glue, srlua

# 2. VSCode的安装
官方网站：https://code.visualstudio.com/ ,在官网选择自己系统对应的安装包下载完成后根据提示安装完成即可。

在安装完成后，为方便后续使用，建议安装一些常用的扩展。包括：
- Chinese (Simplified) (简体中文) Language Pack for Visual Studio Code
- C/C++
- C/C++ Extension Pack
- C/C++ Themes
- Lua
上述扩展在VSCode左侧扩展处逐一搜索后点击安装即可。

# 3. mingw64的安装
## 3.1 mingw64的下载
目前我在官网转到souceforge的页面后一直找不到正确的下载位置，所以我在github找到的下载。链接为 https://github.com/niXman/mingw-builds-binaries/releases ，可以在其中找到适合自己系统的压缩包。该仓库也提供了下载器，可自行探索。

本次我下载的是[mingw-x86_64-14.2.0-release-posix-seh-ucrt-rt_v12-rev0.7z]压缩包。
## 3.2 mingw64的安装
下载完成解压后，将整体文件夹存放至电脑中的一个固定位置，并打开bin文件夹，复制文件夹上部的文件路径。一般为：C:\minGW\mingw64\bin。复制路径后，将该路径更新至系统环境变量中。具体为：在我的电脑 -> 右键 -> 属性 -> 高级系统设置 -> 环境变量 -> 系统变量 Path -> 编辑 -> 新建 -> 将刚才的文件路径粘贴后确认退出即可。

设置完成后，可win+R, 输入cmd打开命令行工具，并输入指令gcc -v，可看到响应。
```
C:\Users\Administrator>gcc -v

(一堆复杂的内容)

gcc version 14.2.0 (x86_64-posix-seh-rev0, Built by MinGW-Builds project)
```
# 4. Lua的安装及配置
## 4.1 Lua的下载
官方网站：https://www.lua.org/

Dowmload -> get a binary -> History 选择最新的release -> 跳转到sourceforge -> Tools Executables -> 选择适合自己系统的压缩包，默认情况下是win64。

如果上述关键字无法在网页内找到，可以直接在网页内ctrl+f后搜索关键字。

本次我下载的是[lua-5.4.2_Win64_bin]压缩包,里面包含lua54.dll、lua54.exe、luac54.exe和wlua54.exe。

## 4.2 Lua的安装
4.1部分下载的安装包解压后将内部4个文件统一放到电脑中的一个固定位置，并复制该文件路径。复制路径后，将该路径更新至系统环境变量中。具体为：在我的电脑 -> 右键 -> 属性 -> 高级系统设置 -> 环境变量 -> 系统变量 Path -> 编辑 -> 新建 -> 将刚才的文件路径粘贴后确认退出即可。

设置完成后，可win+R, 输入cmd打开命令行工具，并输入指令lua54，可看到响应。

```
C:\Users\Administrator>lua54

Lua 5.4.2  Copyright (C) 1994-2020 Lua.org, PUC-Rio
```

## 4.3 Lua库的下载
官方网站：https://www.lua.org/

Dowmload -> get a binary -> History 选择最新的release -> 跳转到sourceforge -> Windows Libraries -> Dynaminc -> 选择适合自己系统的压缩包，默认情况下是win64。

如果上述关键字无法在网页内找到，可以直接在网页内ctrl+f后搜索关键字。

本次我下载的是[lua-5.4.2_Win64_dll17_lib]压缩包,里面包含include文件夹、lua54.dll（动态库文件）和lua54.lib（静态库文件）。压缩包解压后准备使用即可。


# 5. IUP库
官方网站：https://iup.sourceforge.net/

下载链接：http://sourceforge.net/projects/iup/files/3.31/

Windows Libraries -> Dynamic -> 选择适合自己系统的压缩包，默认情况下是win64。

如果上述关键字无法在网页内找到，可以直接在网页内ctrl+f后搜索关键字。

本次我下载的是[lua-5.4.2_Win64_dll17_lib]压缩包，内部包含：
- include文件夹，里面包含了相关头文件
- Lua54\Lua53\Lua52\Lua51这4个文件夹为空文件夹，不必理会
- 其他为动态库.dll文件，后续按需取用即可

# 6. srlua
## 6.1 下载
github仓库地址：https://github.com/LuaDist/srlua.git， 链接点开后，点击**Code -> Download ZIP**即可。

国内仓库地址：https://gitcode.com/Haoriwa/srlua/overview， 链接点开后，点击**下载ZIP**即可。

## 6.2 编译
在下载后的文件夹中有CMakeLists.txt和Makefile文件。这两个文件应该是辅助编译的。但是我对这些工具不了解，所以是自己手动编译的。

### 6.2.1 编译glue
glue的编译较为简单，在安装完成gcc后即可直接完成编译，没有其他文件依赖。在项目文件srlua中右键->在终端中打开，之后在弹出的终端中运行以下指令，即可生成glue.exe文件。

```
gcc -o glue.exe glue.c
```
### 6.2.2 编译srlua
srlua的编译相对glue复杂一点，因为需要准备相关的依赖头文件和库文件。我也是通过解决这些问题，对编译指令中的一些指令有了基本的认识。

#### 6.2.2.1 简单粗暴的方法
在项目文件srlua中右键->在终端中打开，输入以下终端指令：
```
gcc -o srlua.exe srlua.c
```
会报出缺少lua.h头文件。在项目文件夹中补充后还会报出缺失其他头文件，依次为：luaconf.h,lualib,lauxlib。将上述4个头文件找到，并复制到项目文件夹中，与srlua.c文件放在同一层级，即可解决上述问题。

继续执行开始的gcc指令，这次开始报出undefined reference to的错误。这是因为缺少库文件导致的。因此，需要将出错相关的库文件同样放到与srlua.c文件同一层级。并将指令改写为：
```
gcc -o srlua.exe srlua.c -L. -llua54 -ldl
```
其中-L的意思为向编译器指明库文件的路径（即所在的文件夹），-L后的"."的意思为当前终端所在路径。在当前场景下就是srlua.c文件所在路径。-l指令的意思为告诉编译器具体的库文件的名称。即：-llua54就代表着库文件名字为lua54；-ldl代表着库文件名字为libdl。

上述指令执行后，文件夹内即可生成srlua.exe文件。

#### 6.2.2.2 "文明"一点的方法
通过上述的解释可以知道，头文件和库文件不一定要放在当前的文件夹，我们可以通过改变编译指令告诉编译器这些文件在哪里。除了上述的-L和-l指令外，头文件可以通过-I指令告知头文件路径。例如：
```
gcc -o srlua.exe srlua.c -Ixxxx -L. -llua54 -ldl
```
这样，我们的头文件和库文件不必复制进该文件夹内，统一在include和lib文件夹内管理即可。

# 7.实例
## 7.1 Lua脚本文件编写
在6 srlua的glue.exe和srlua.exe的文件夹中新建文件，文件名为HelloWorld.lua。

这里我们采用IUP库的HelloWorld示例代码，其代码如下：

```Lua
require("iuplua")

iup.Message("Hello World 1","Hello world from IUP.")

-- to be able to run this script inside another context
if (iup.MainLoopLevel()==0) then
  iup.MainLoop()
  iup.Close()
end
```

复制进文件后保存关闭即可。之后，再把以下动态库dll文件存放至该文件夹：
- iup.dll
- iuplua54.dll
- lua54.dll
上述文件在前面的库文件下载中都已完成下载，直接找到后复制即可。

## 7.2 生成exe
在srlua文件夹中右键，选择在终端中打开，并输入以下指令：
```
glue.exe srlua.exe xxx.lua xxx.exe
```
其中，xxx.lua为准备编译的脚本文件，xxx.exe为编译完成后准备生成的.exe文件。终端运行后报出了<找不到命令 glue.exe，但它确实存在于当前位置。默认情况下，Windows PowerShell 不会从当前位置加载命令。如果信任此命令，请改为键入“.\glue.exe”。有关详细信息，请参阅 "get-help about_Command_Precedence"。>的建议。按照这个建议更改后：
```
.\glue.exe srlua.exe xxx.lua xxx.exe
```
就可以生成想要的exe文件了！

**目前有一个问题没有解决，就是执行exe文件后会同时弹出终端，还在摸索。**

# 后记
其实想要实现的软件功能并不难，只是因为是第一次做这种“开发”工作，导致每一步都出现了问题。但是可喜的是坚持下来了。

在这个过程中曾经中途不想继续了，转头去找别的开发框架或者工具去了。但是，后面发现每一个工具或框架都有各自的难点。所以，最终还是在这样一个工具链上坚持下去了。

在我的工作过程中不需要开发太复杂、太繁琐的软件。我只是想要自己做一些简单的工具，让一些工作流能更方便、更快一点就好了。

编程确实很有意思。